* [ ] Define endpoints
  * [x] Foresee api versioning
  * [x] User resource
    * [x] Single user management
      * [x] Get user details
        * GET - **/user/:username**
      * [x] Create a new user
        * POST - **/user**
      * [x] Update existing user
        * PUT - **/user/:username**
      * [x] Delete existing user
        * DELETE = **/user/:username**
    * [x] User contacts
      * [x] Get user contacts
        * GET - **/user/:username/contacts**
      * [x] Add user contact
        * POST - **/user/:username/contacts**
      * [x] Update user contact
        * PUT - **/user/:username/contacts/:contactId**
      * [x] Delete user contact
        * DELETE - **/user/:username/contacts/:contactId**
    * [x] User skills
      * [x] Get user skills
        * GET - **/user/:username/skills**
      * [x] Add user skill
        * POST - **/user/:username/skills**
      * [x] Update user skill
        * PUT - **/user/:username/skills/:skillId**
      * [x] Delete user skill
        * DELETE - **/user/:username/skills/:skillId**
    * [x] User experience
      * [x] Get user experience
        * GET - **/user/:username/experience**
      * [x] Add user experience
        * POST - **/user/:username/experience**
      * [x] Update user experience
        * PUT - **/user/:username/experience/:experienceId**
      * [x] Delete user experience
        * DELETE - **/user/:username/experience/:experienceId**
    * [x] User education
      * [x] Get user education
        * GET - **/user/:username/education**
      * [x] Add user education
        * POST - **/user/:username/education**
      * [x] Update user education
        * PUT - **/user/:username/education/:educationId**
      * [x] Delete user education
        * DELETE - **/user/:username/education/:educationId**
    * [x] User projects
      * [x] Get user projects
        * GET - **/user/:username/projects**
      * [x] Add user project
        * POST - **/user/:username/projects**
      * [x] Update user project
        * PUT - **/user/:username/projects/:projectId**
      * [x] Delete user project
        * DELETE - **/user/:username/projects/:projectId**
    * [x] User languages
      * [x] Get user languages
        * GET - **/user/:username/languages**
      * [x] Add user language
        * POST - **/user/:username/languages**
      * [x] Delete user language
        * DELETE - **/user/:username/languages/:languageId**
    * [x] User resumes
      * [x] Get user resumes
        * GET - **/user/:username/resumes**
      * [x] Add user resume
        * POST - **/user/:username/resumes**
      * [x] Update user resume
        * PUT - **/user/:username/resumes/:resumeId**
      * [x] Delete user resume
        * DELETE - **/user/:username/resumes/:resumeId**