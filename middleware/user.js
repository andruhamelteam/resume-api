const { User, Language } = require('resume-db-persistence');

const middleware = (req, res, next) => {
  User.scope(
    { method: ['general', req.params.username] },
  ).findOne({
    include: [{
      model: Language
    }]
  }).then((user) => {
    if (user === null) {
      res.status(404).json({
        error: `User ${req.params.username} not found.`
      });
    }

    req.User = user;

    next();
  }).catch(next);
}

module.exports = middleware;