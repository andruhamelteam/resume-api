const { UserLanguage, Language } = require('resume-db-persistence');

const toJson = (language) => {
  return {
    id: language.UserLanguage.id,
    name: language.name
  };
};

const get = (req, res, next) => {
  req.User.getLanguages().then((languages) => {
    return languages.map(toJson);
  }).then((languages) => {
    res.json(languages);
  }).catch(next);
};

const create = (req, res, next) => {
  Language.findOne({
    where: {
      name: req.body.name
    }
  }).then((language) => {
    if (language === null) {
      res.status(404).json({
        error: `Language ${req.body.name} not found.`
      });
    }

    req.User.addLanguage(language);

    return language;
  }).then((language) => {
    return req.User.reload().then(
      () => Promise.resolve(language)
    );
  }).then((language) => {
    return req.User.getLanguages().then((languages) => {
      return Promise.resolve(
        languages.reduce((accumulator, value) => {
          return value.id === language.id ? value : accumulator;
        }, null)
      );
    });
  }).then((language) => {
    res.json(toJson(language));
  }).catch(next);
};

const remove = (req, res, next) => {
  UserLanguage.findOne({
    UserId: req.User.id,
    id: req.params.languageId
  }).then((language) => {
    if (language === null) {
      res.status(404).json({
        error: `Language with id ${req.params.languageId} not found.`
      });
    }

    return language.destroy();
  }).then(() => {
    res.status(204).end();
  }).catch(next);
}

module.exports = {
  get,
  create,
  remove
}