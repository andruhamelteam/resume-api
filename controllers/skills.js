const {Skill, SkillType} = require('resume-db-persistence');

const get = (req, res, next) => {
  Skill.scope('general').findAll({
    UserId: req.User.id
  }).then((skills) => {
    res.json(skills);
  }).catch(next);
};

const create = (req, res, next) => {
  SkillType.findOne({
    type: req.body.type
  }).then((skillType) => {
    if (skillType === null) {
      res.status(404).json({
        error: `SkillType ${req.body.type} not found.`
      });
    }

    return skillType;
  }).then((skillType) => {
    const skill = Skill.build(
      req.body,
      {
        fields: ['value']
      }
    )

    skill.setUser(req.User);
    skill.setSkillType(skillType);

    return skill.save();
  }).then((skill) => {
    return Skill.scope('general').findById(skill.id);
  }).then((skill) => {
    res.json(skill);
  }).catch(next);
}

const update = (req, res, next) => {
  SkillType.findOne({
    where: {
      type: req.body.type
    }
  }).then((skillType) => {
    if (skillType === null) {
      res.status(404).json({
        error: `SkillType ${req.body.type} not found.`
      });
    }

    return Skill.findOne({
      where: {
        id: req.params.skillId,
        UserId: req.User.id
      }
    }).then((skill) => {
      if (skill === null) {
        res.status(404).json({
          error: `Skill with id ${req.params.skillId} not found.`
        });
      }

      skill.setSkillType(skillType);
      skill.value = req.body.value;

      return skill.save();
    })
  }).then((skill) => {
    return Skill.scope('general').findById(skill.id);
  }).then((skill) => {
    res.json(skill);
  }).catch(next);
}

const remove = (req, res,next) => {
  Skill.findOne({
    where: {
      id: req.params.skillId,
      UserId: req.User.id
    }
  }).then((skill) => {
    if (skill === null) {
      res.status(404).end();
    }

    return skill.destroy();
  }).then(() => {
    res.status(204).end();
  }).catch(next);
}

module.exports = {
  get,
  create,
  update,
  remove
}