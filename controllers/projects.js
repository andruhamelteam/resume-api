const { Project, Technology, ProjectCharacteristic, Sequelize } = require('resume-db-persistence');

const findOrCreateTechnologyModels = ({ technologies }) => {
  if (technologies && Array.isArray(technologies)) {
    return Promise.all(
      technologies.map((val) => {
        return Technology.findOrCreate({
          where: Sequelize.where(
            Sequelize.fn('lower', Sequelize.col('name')),
            val.toLowerCase()
          ),
          defaults: {
            name: val
          }
        }).spread((technology) => technology);
      })
    );
  }

  return [];
};

const getCharactericticsJson = ({ characteristics }) => {
  if (characteristics && Array.isArray(characteristics)) {
    return characteristics.map(
      (val) => ({ value: val })
    );
  }

  return [];
};

const updateProjectCharacterictics = ({ characteristics }, project) => {
  if (characteristics && Array.isArray(characteristics)) {

    return project.getProjectCharacteristics().then((items) => {
      let toRemove = [];

      items.forEach((characteristic) => {
        let itemIndex = characteristics.indexOf(characteristic.value);
        if (itemIndex !== -1) {
          characteristics.splice(itemIndex, 1);
        } else {
          toRemove.push(characteristic);
        }
      });

      let toRemovePromises = toRemove.map((item) => item.destroy());
      let toInsertPromises = characteristics.map((item) => {
        const characteristic = ProjectCharacteristic.build({
          value: item
        });

        characteristic.setProject(project, { save: false });
        return characteristic.save();
      });
    });

    return Promise.all([].concat(toRemovePromises, toInsertPromises));
  }

  return Promise.resolve();
};

const get = (req, res, next) => {
  Project.scope('general').findAll({
    where: {
      UserId: req.User.id
    }
  }).then((projects) => {
    res.json(projects);
  }).catch(next);
};

const create = (req, res, next) => {
  findOrCreateTechnologyModels(req.body).then((technologies) => {
    const project = Project.build(
      {
        name: req.body.name,
        ProjectCharacteristics: getCharactericticsJson(req.body)
      },
      {
        include: [ProjectCharacteristic]
      }
    );

    project.setUser(req.User, { save: false });

    return project.save().then(
      (project) => Promise.resolve([project, technologies])
    );
  }).then(([project, technologies]) => {
    return project.setTechnologies(technologies).then(
      () => Promise.resolve(project)
    );
  }).then((project) => {
    return Project.scope('general').findById(project.id);
  }).then((project) => {
    res.json(project);
  }).catch(next);
};

const update = (req, res, next) => {
  Project.findOne({
    where: {
      UserId: req.User.id,
      id: req.params.projectId
    }
  }).then((project) => {
    if (project === null) {
      res.status(404).json({
        error: `Project with id ${req.params.projectId} not found.`
      })
    }

    project.name = req.body.name;

    return project.save();
  }).then((project) => {
    return findOrCreateTechnologyModels(req.body).then(
      (technologies) => {
        return Promise.resolve([project, technologies])
      }
    )
  }).then(([project, technologies]) => {
    return project.setTechnologies(technologies).then(
      () => Promise.resolve(project)
    );
  }).then((project) => {
    return updateProjectCharacterictics(
      req.body,
      project
    ).then(
      () => Promise.resolve(project)
    );
  }).then((project) => {
    return Project.scope('general').findById(project.id);
  }).then((project) => {
    res.json(project);
  }).catch(next);
}

const remove = (req, res, next) => {
  Project.findOne({
    where: {
      UserId: req.User.id,
      id: req.params.projectId
    }
  }).then((project) => {
    if (project === null) {
      res.status(404).json({
        error: `Project with id ${req.params.projectId} not found.`
      });
    }

    return project.destroy();
  }).then(() => {
    res.status(204).end();
  }).catch(next);
};

module.exports = {
  get,
  create,
  update,
  remove
}