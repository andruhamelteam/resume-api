const { Education, EducationalOrganization, Sequelize } = require('resume-db-persistence');

const get = (req, res, next) => {
  Education.scope('general').findAll({
    UserId: req.User.id
  }).then((education) => {
    res.json(education);
  }).catch(next);
}

const create = (req, res, next) => {
  EducationalOrganization.findOrCreate({
    where: Sequelize.where(
      Sequelize.fn('lower', Sequelize.col('name')),
      req.body.organization.toLowerCase()
    ),
    defaults: {
      name: req.body.organization
    }
  }).spread((organization) => {
    const education = Education.build(
      req.body,
      {
        fields: ['from', 'to', 'qualification', 'course']
      }
    );

    education.setUser(req.User);
    education.setEducationalOrganization(organization);

    return education.save();
  }).then((education) => {
    return Education.scope('general').findById(education.id);
  }).then((education) => {
    res.json(education);
  }).catch(next);
}

const update = (req, res, next) => {
  EducationalOrganization.findOrCreate({
    where: Sequelize.where(
      Sequelize.fn('lower', Sequelize.col('name')),
      req.body.organization.toLowerCase()
    ),
    defaults: {
      name: req.body.organization
    }
  }).spread((organization) => {
    return Education.findOne({
      where: {
        UserId: req.User.id,
        id: req.params.educationId
      }
    }).then((education) => {
      return Promise.resolve([organization, education]);
    });
  }).spread((organization, education) => {
    if (education === null) {
      res.status(404).json({
        error: `Education with id ${req.params.educationId} not found.`
      });
    }

    education.from = req.body.from;
    education.to = req.body.to;
    education.qualification = req.body.qualification;
    education.course = req.body.course;
    education.setUser(req.User);
    education.setEducationalOrganization(organization);

    return education.save();
  }).then((education) => {
    return Education.scope('general').findById(education.id);
  }).then((education) => {
    res.json(education);
  }).catch(next);
}

const remove = (req, res, next) => {
  Education.findOne({
    where: {
      UserId: req.User.id,
      id: req.params.educationId
    }
  }).then((education) => {
    if (education === null) {
      res.status(404).json({
        error: `Education with id ${req.params.educationId} not found.`
      });
    }

    return education.destroy();
  }).then(() => {
    res.status(204).end();
  }).catch(next);
}

module.exports = {
  get,
  create,
  update,
  remove
}