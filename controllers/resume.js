const { Resume, JobPosition, Contact, Skill,
  Experience, Project, Education,
  UserLanguage, Language, Sequelize } = require('resume-db-persistence');

const getAssociations = (model, associations, resume, req) => {
  if (associations && Array.isArray(associations)) {
    return Promise.all(
      associations.map((association) => {
        return model.findOne({
          where: {
            UserId: req.User.id,
            id: association
          }
        })
      })
    ).then((associations) => Promise.resolve([
      resume,
      associations.filter((val) => val !== null)
    ]));
  }

  return Promise.resolve([resume, []]);
}

const get = (req, res, next) => {
  Resume.scope('general').findAll({
    where: {
      UserId: req.User.id
    }
  }).then((resumes) => {
    res.json(resumes);
  }).catch(next);
};

const getOne = (req, res, next) => {
  Resume.scope('general').findById(req.params.resumeId).then((resume) => {
    if (resume === null) {
      res.status(404).json({
        error: `Resume with id ${req.params.resumeId} not found.`
      })
    }

    res.json(resume);
  }).catch(next);
}

const create = (req, res, next) => {
  JobPosition.findOrCreate({
    where: Sequelize.where(
      Sequelize.fn('lower', Sequelize.col('name')),
      req.body.position
    ),
    defaults: {
      name: req.body.position
    }
  }).spread((jobPosition) => {
    const resume = Resume.build(
      req.body,
      {
        fields: ['name', 'summary']
      }
    );

    resume.setUser(req.User, { save: false });
    resume.setJobPosition(jobPosition, { save: false });

    return resume.save();
  }).then((resume) => {
    return getAssociations(
      Contact,
      req.body.contacts ? req.body.contacts : [],
      resume,
      req
    ).then(([resume, contacts]) => {
      return resume.setContacts(contacts);
    }).then(() => Promise.resolve(resume));
  }).then((resume) => {
    return getAssociations(
      Skill,
      req.body.skills ? req.body.skills : [],
      resume,
      req
    ).then(([resume, skills]) => {
      return resume.setSkills(skills);
    }).then(() => Promise.resolve(resume));
  }).then((resume) => {
    return getAssociations(
      Experience,
      req.body.experience ? req.body.experience : [],
      resume,
      req
    ).then(([resume, experience]) => {
      return resume.setExperiences(experience);
    }).then(() => Promise.resolve(resume));
  }).then((resume) => {
    return getAssociations(
      Project,
      req.body.projects ? req.body.projects : [],
      resume,
      req
    ).then(([resume, projects]) => {
      return resume.setProjects(projects);
    }).then(() => Promise.resolve(resume));
  }).then((resume) => {
    return getAssociations(
      Education,
      req.body.education ? req.body.education : [],
      resume,
      req
    ).then(([resume, education]) => {
      return resume.setEducation(education);
    }).then(() => Promise.resolve(resume));
  }).then((resume) => {
    return getAssociations(
      UserLanguage,
      req.body.languages ? req.body.languages : [],
      resume,
      req
    ).then(([resume, languages]) => {
      return Promise.all(
        languages.map((lang) => Language.findById(lang.LanguageId))
      ).then(
        (languages) => Promise.resolve([resume, languages])
      );
    }).then(([resume, languages]) => {
      return resume.setLanguages(languages);
    }).then(() => Promise.resolve(resume));
  }).then((resume) => {
    return Resume.scope('general').findById(resume.id);
  }).then((resume) => {
    res.json(resume);
  }).catch(next);
};

const update = (req, res, next) => {
  JobPosition.findOrCreate({
    where: Sequelize.where(
      Sequelize.fn('lower', Sequelize.col('name')),
      req.body.position
    ),
    defaults: {
      name: req.body.position
    }
  }).spread((jobPosition) => {
    return Resume.findOne({
      where: {
        UserId: req.User.id,
        id: req.params.resumeId
      }
    }).then((resume) => {
      if (resume === null) {
        res.status(404).json({
          error: `Resume with id ${req.params.resumeId} not found.`
        })
      }

      return Promise.resolve([resume, jobPosition]);
    });
  }).then(([resume, jobPosition]) => {
    resume.name = req.body.name;
    resume.summary = req.body.summary;
    resume.setUser(req.User, { save: false });
    resume.setJobPosition(jobPosition, { save: false });

    return resume.save();
  }).then((resume) => {
    return getAssociations(
      Contact,
      req.body.contacts ? req.body.contacts : [],
      resume,
      req
    ).then(([resume, contacts]) => {
      return resume.setContacts(contacts);
    }).then(() => Promise.resolve(resume));
  }).then((resume) => {
    return getAssociations(
      Skill,
      req.body.skills ? req.body.skills : [],
      resume,
      req
    ).then(([resume, skills]) => {
      return resume.setSkills(skills);
    }).then(() => Promise.resolve(resume));
  }).then((resume) => {
    return getAssociations(
      Experience,
      req.body.experience ? req.body.experience : [],
      resume,
      req
    ).then(([resume, experience]) => {
      return resume.setExperiences(experience);
    }).then(() => Promise.resolve(resume));
  }).then((resume) => {
    return getAssociations(
      Project,
      req.body.projects ? req.body.projects : [],
      resume,
      req
    ).then(([resume, projects]) => {
      return resume.setProjects(projects);
    }).then(() => Promise.resolve(resume));
  }).then((resume) => {
    return getAssociations(
      Education,
      req.body.education ? req.body.education : [],
      resume,
      req
    ).then(([resume, education]) => {
      return resume.setEducation(education);
    }).then(() => Promise.resolve(resume));
  }).then((resume) => {
    return getAssociations(
      UserLanguage,
      req.body.languages ? req.body.languages : [],
      resume,
      req
    ).then(([resume, languages]) => {
      return Promise.all(
        languages.map((lang) => Language.findById(lang.LanguageId))
      ).then(
        (languages) => Promise.resolve([resume, languages])
      );
    }).then(([resume, languages]) => {
      return resume.setLanguages(languages);
    }).then(() => Promise.resolve(resume));
  }).then((resume) => {
    return Resume.scope('general').findById(resume.id);
  }).then((resume) => {
    res.json(resume);
  }).catch(next);
};

const remove = (req, res, next) => {
  Resume.findOne({
    where: {
      UserId: req.User.id,
      id: req.params.resumeId
    }
  }).then((resume) => {
    if (resume === null) {
      res.status(404).json({
        error: `Resume with id ${req.params.resumeId} not found.`
      })
    }

    return resume.destroy();
  }).then(() => {
    res.status(204).end();
  }).catch(next);
};

module.exports = {
  get,
  getOne,
  create,
  update,
  remove
}