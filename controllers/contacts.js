const { Contact, ContactType } = require('resume-db-persistence');

const get = (req, res, next) => {
  Contact.scope('general').findAll({
    UserId: req.User.id
  }).then((contacts) => {
    res.json(contacts);
  }).catch(next);
};

const create = (req, res, next) => {
  ContactType.findOne({
    where: {
      type: req.body.type
    }
  }).then((contactType) => {
    if (contactType === null) {
      res.status(404).json({
        error: `ContactType ${req.body.type} not found.`
      });
    }

    const contact = Contact.build(
      req.body,
      {
        fields: ['value']
      }
    );

    contact.setContactType(contactType);
    contact.setUser(req.User);

    return contact.save();
  }).then((contact) => {
    return Contact.scope('general').findById(contact.id);
  }).then((contact) => {
    res.json(contact);
  }).catch(next);
};

const update = (req, res, next) => {
  ContactType.findOne({
    where: {
      type: req.body.type
    }
  }).then((contactType) => {
    if (contactType === null) {
      res.status(404).json({
        error: `ContactType ${req.body.type} not found.`
      });
    }

    return Contact.findOne({
      where: {
        id: req.params.contactId,
        UserId: req.User.id
      }
    }).then((contact) => {
      if (contact === null) {
        res.status(404).json({
          error: `Contact with id ${req.params.contactId} not found.`
        });
      }

      contact.setContactType(contactType);
      contact.value = req.body.value;

      return contact.save()
    })
  }).then((contact) => {
    return Contact.scope('general').findById(contact.id);
  }).then((contact) => {
    res.json(contact);
  }).catch(next);
};

const remove = (req, res, next) => {
  Contact.findOne({
    where: {
      id: req.params.contactId,
      UserId: req.User.id
    }
  }).then((contact) => {
    if (contact === null) {
      res.status(404).end();
    }

    return contact.destroy();
  }).then(() => {
    res.status(204).end();
  }).catch(next);
};

module.exports = {
  get,
  create,
  update,
  remove
}