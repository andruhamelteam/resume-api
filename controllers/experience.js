const { Experience, JobPosition, Employer, Responsibility, Sequelize } = require('resume-db-persistence');

const getResponsibiliesModels = (({ responsibilities }) => {
  var responsibilityModels = [];

  if (responsibilities && Array.isArray(responsibilities)) {
    responsibilityModels = responsibilities.map((responsibility) => {
      return Responsibility.build({
        value: responsibility
      })
    });
  }

  return Promise.resolve(responsibilityModels);
});

const findOrCreateResponsibilitiesModels = (({ responsibilities }, experience) => {
  if (responsibilities && Array.isArray(responsibilities)) {
    return Promise.all(
      responsibilities.map((responsibility) => {
        return Responsibility.findOrCreate({
          where: {
            [Sequelize.Op.and]: [
              {experienceId: experience.id},
              Sequelize.where(
                Sequelize.fn('lower', Sequelize.col('value')),
                responsibility.toLowerCase()
              )
            ]            
          },
          defaults: {
            ExperienceId: experience.id,
            value: responsibility
          }
        })
      })
    );
  }

  return Promise.resolve([]);
});

const get = (req, res, next) => {
  Experience.scope('general').findAll({
    UserId: req.User.id
  }).then((experience) => {
    res.json(experience);
  }).catch(next);
};

const create = (req, res, next) => {
  JobPosition.findOrCreate({
    where: Sequelize.where(
      Sequelize.fn('lower', Sequelize.col('name')),
      req.body.position.toLowerCase()
    ),
    defaults: {
      name: req.body.position
    }
  }).spread((jobPosition, created) => {
    return Employer.findOrCreate({
      where: Sequelize.where(
        Sequelize.fn('lower', Sequelize.col('name')),
        req.body.employer.toLowerCase()
      ),
      defaults: {
        name: req.body.employer
      }
    }).spread((employer, created) => {
      return Promise.resolve([jobPosition, employer]);
    })
  }).spread((jobPosition, employer) => {
    const experience = Experience.build(
      req.body,
      {
        fields: ['from', 'to']
      }
    );

    experience.setUser(req.User, { save: false });
    experience.setJobPosition(jobPosition, { save: false });
    experience.setEmployer(employer, { save: false });

    return experience.save();
  }).then((experience) => {
    return getResponsibiliesModels(
      req.body
    ).then((responsibilities) => {
      return Promise.all(
        responsibilities.map((responsibility) => {
          responsibility.setExperience(experience, { save: false });

          return responsibility.save();
        })
      );
    }).then(() => {
      return Promise.resolve(experience);
    });
  }).then((experience) => {
    return Experience.scope('general').findById(experience.id);
  }).then((experience) => {
    res.json(experience);
  }).catch(next);
}

const update = (req, res, next) => {
  JobPosition.findOrCreate({
    where: Sequelize.where(
      Sequelize.fn('lower', Sequelize.col('name')),
      req.body.position.toLowerCase()
    ),
    defaults: {
      name: req.body.position
    }
  }).spread((jobPosition, created) => {
    return Employer.findOrCreate({
      where: Sequelize.where(
        Sequelize.fn('lower', Sequelize.col('name')),
        req.body.employer.toLowerCase()
      ),
      defaults: {
        name: req.body.employer
      }
    }).spread((employer, created) => {
      return Promise.resolve([jobPosition, employer]);
    })
  }).spread((jobPosition, employer) => {
    return Experience.findOne({
      where: {
        UserId: req.User.id,
        id: req.params.experienceId
      }
    }).then((experience) => {
      return Promise.resolve([jobPosition, employer, experience]);
    });
  }).spread((jobPosition, employer, experience) => {
    return findOrCreateResponsibilitiesModels(req.body, experience).then((result) => {
      return result.map((item) => item[0]);
    }).then((responsibilities) => {
      return experience.getResponsibilities().then((existingResponsibilities) => {
        var toDelete = existingResponsibilities.filter((responsibility) => {
          for (let i = 0; i < responsibilities.length; i++) {
            const element = responsibilities[i];
          
            if (element.id === responsibility.id) {
              return false;
            }
          }

          return true;
        });

        toDelete.map((item) => item.destroy());

        return Promise.resolve([jobPosition, employer, experience])
      });
    });
  }).spread((jobPosition, employer, experience, responsibilities) => {
    if (experience === null) {
      res.status(404).json({
        error: `Experience with id ${req.params.experienceId} not found.`
      });
    }

    experience.from = req.body.from;
    experience.to = req.body.to;
    experience.setJobPosition(jobPosition, { save: false });
    experience.setEmployer(employer, { save: false });

    return experience.save();
  }).then((experience) => {
    return Experience.scope('general').findById(experience.id);
  }).then((experience) => {
    res.json(experience);
  }).catch(next);
}

const remove = (req, res, next) => {
  Experience.findOne({
    where: {
      UserId: req.User.id,
      id: req.params.experienceId
    }
  }).then((experience) => {
    if (experience === null) {
      res.status(404).json({
        error: `Experience with id ${req.params.experienceId} not found.`
      })
    }

    return experience.destroy();
  }).then(() => {
    res.status(204).end();
  }).catch(next);
}

module.exports = {
  get,
  create,
  update,
  remove
}