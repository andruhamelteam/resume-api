const { User: UserModel, Account: AccountModel } = require('resume-db-persistence');

const get = (req, res, next) => {
  UserModel.scope(
    { method: ['general', req.params.username] }
  ).findOne({}).then((user) => {
    if (user === null) {
      res.status(404).end();
    }

    res.json(
      user.get({
        plain: true
      })
    );
  }).catch(next);
};

const update = (req, res, next) => {
  UserModel.scope(
    { method: ['general', req.params.username] }
  ).findOne({}).then((user) => {
    if (user === null) {
      res.status(404).end();
    }

    user.firstName = req.body.firstName;
    user.lastName = req.body.lastName;

    return user.save();
  }).then((user) => {
    res.json(user.toJSON());
  }).catch(next);
};

const remove = (req, res, next) => {
  UserModel.scope(
    { method: ['general', req.params.username] }
  ).findOne({}).then((user) => {
    if (user === null) {
      res.status(404).end();
    }

    return user;
  }).then((user) => {
    return AccountModel.findOne({
      where: {
        login: user.Account.login
      }
    });
  }).then((account) => {
    return account.destroy();
  }).then(() => {
    res.status(204).end();
  }).catch(next);
};

const create = (req, res, next) => {
  AccountModel.create({
    login: `${req.body.firstName}-${req.body.lastName}`.toLowerCase(),
    password: `${req.body.firstName}-${req.body.lastName}`.toLowerCase()
  }).then((account) => {
    const user = UserModel.build(
      req.body,
      {
        fields: ['firstName', 'lastName']
      }
    );

    user.setAccount(account);

    return user.save();
  }).then((user) => {
    return user.getAccount();
  }).then((account) => {    
    return UserModel.scope(
      { method: ['general', account.login] }
    ).findOne({});
  }).then((user) => {
    res.json(user.toJSON());
  }).catch(next);
};

module.exports = {
  get,
  create,
  update,
  remove
}