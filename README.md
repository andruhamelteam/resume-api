# resume-api

RESTful API for resume application.

## How to setup the API
The API comes with docker environment that provides the following services:

 - api - service with node.js:8.9 that run API itself;
 - db - MariaDB RDBMS;
 - migrations - service that provides database migrations and seed to bring db to some initial state and seed with some test data;
 - phpmyadmim - phpMyAdmin database tool.

#### To install dependencies:

    docker run -it --rm -v $(pwd):/app -w /app node:8.9 npm install

#### To run docker-compose services:

    docker-compose up -d
The command brings up all services. The migrations service will run all migrations on database services to create all necessary tables. If you need some test data the seeds can be run manually as follow:

    docker-compose run --rm migrations /app/node_modules/.bin/sequelize db:seed:all

#### How to access docker services:
* API - [http://localhost:8501](http://localhost:8501)
* MariaDB database - localhost:8506 (username: root, password: rand0m!)
* phpMyAdmin - [http://localhost:8580/](http://localhost:8580/)

## Available resources:
All resorces resides under http://localhost:8580/v1/  

### User resource
#### Single user management
##### Get user details
*GET - **/user/:username***

**Sample response:***
```json
{
    "id": 1,
    "firstName": "Demo",
    "lastName": "User",
    "Account": {
        "login": "demo-user"
    }
}
```

##### Create a new user
*POST - **/user***

**Sample request:***
```json
{
  "firstName": "API",
  "lastName": "User"
}
```

**Sample response:***
```json
{
    "id": 2,
    "firstName": "API",
    "lastName": "User",
    "Account": {
        "login": "api-user"
    }
}
```

##### Update existing user
*PUT - **/user/:username***

**Sample request:***
```json
{
    "firstName": "API",
    "lastName": "User"
}
```

**Sample response:***
```json
{
    "id": 2,
    "firstName": "API",
    "lastName": "User",
    "Account": {
        "login": "api-user"
    }
}
```

##### Delete existing user
*DELETE = **/user/:username***

No response for the route. Status 204 No Content on success.

#### User contacts
##### Get user contacts
*GET - **/user/:username/contacts***

**Sample response:**
```json
[
    {
        "id": 2,
        "value": "demo-user@mail.com",
        "ContactType": {
            "id": 3,
            "type": "e-mail"
        }
    },
    {
        "id": 1,
        "value": "Lviv, Ukraine",
        "ContactType": {
            "id": 1,
            "type": "address"
        }
    }
]
```
##### Add user contact
*POST - **/user/:username/contacts***

**Sample request:**
```json
{
	"type": "skype",
	"value": "api-contact-78"
}
```

**Sample response:**
```json
{
    "id": 3,
    "value": "api-contact-78",
    "ContactType": {
        "id": 4,
        "type": "skype"
    }
}
```

##### Update user contact
*PUT - **/user/:username/contacts/:contactId***

**Sample request:**
```json
{
	"type": "skype",
	"value": "api-contact-renamed-2"
}
```

**Sample response:**
```json
{
    "id": 3,
    "value": "api-contact-renamed-2",
    "ContactType": {
        "id": 4,
        "type": "skype"
    }
}
```
##### Delete user contact
*DELETE - **/user/:username/contacts/:contactId***

No response for the route. Status 204 No Content on success.

#### User skills
##### Get user skills
*GET - **/user/:username/skills***

**Sample response:**
```json
[
    {
        "id": 2,
        "value": "JavaScript",
        "SkillType": {
            "id": 1,
            "type": "languages"
        }
    },
    {
        "id": 1,
        "value": "PHP",
        "SkillType": {
            "id": 1,
            "type": "languages"
        }
    }
]
```

##### Add user skill
*POST - **/user/:username/skills***

**Sample request:**
```json
{
	"type": "languages",
	"value": "vanilla-js"
}
```

**Sample response:**
```json
{
    "id": 3,
    "value": "vanilla-js",
    "SkillType": {
        "id": 1,
        "type": "languages"
    }
}
```

##### Update user skill
*PUT - **/user/:username/skills/:skillId***

**Sample request:**
```json
{
	"type": "languages",
	"value": "vanilla-js-renamed"
}
```

**Sample response:**
```json
{
    "id": 3,
    "value": "vanilla-js-renamed",
    "SkillType": {
        "id": 1,
        "type": "languages"
    }
}
```

##### Delete user skill
*DELETE - **/user/:username/skills/:skillId***

No response for the route. Status 204 No Content on success.

#### User experience
##### Get user experience
*GET - **/user/:username/experience***

**Sample response:**
```json
[
    {
        "id": 1,
        "from": "2015-01-01T00:00:00.000Z",
        "to": "2018-01-01T00:00:00.000Z",
        "Employer": {
            "id": 1,
            "name": "Vodokanal"
        },
        "JobPosition": {
            "id": 1,
            "name": "Web/UI developer"
        }
    }
]
```

##### Add user experience
*POST - **/user/:username/experience***

**Sample request:**
```json
{
	"position": "Web/UI developer",
	"employer": "Vodokanal",
	"from": "2015-01-01",
	"to": "2018-01-01"
}
```

**Sample response:**
```json
{
    "id": 1,
    "from": "2015-01-01T00:00:00.000Z",
    "to": "2018-01-01T00:00:00.000Z",
    "Employer": {
        "id": 1,
        "name": "Vodokanal"
    },
    "JobPosition": {
        "id": 1,
        "name": "Web/UI developer"
    }
}
```

##### Update user experience
*PUT - **/user/:username/experience/:experienceId***

**Sample request:**
```json
{
	"position": "Web/UI developer-renamed",
	"employer": "Vodokanal-renamed",
	"from": "2015-01-01",
	"to": "2018-12-01"
}
```

**Sample response:**
```json
{
    "id": 1,
    "from": "2015-01-01T00:00:00.000Z",
    "to": "2018-12-01T00:00:00.000Z",
    "Employer": {
        "id": 2,
        "name": "Vodokanal-renamed"
    },
    "JobPosition": {
        "id": 2,
        "name": "Web/UI developer-renamed"
    }
}
```

##### Delete user experience
*DELETE - **/user/:username/experience/:experienceId***

No response for the route. Status 204 No Content on success.

#### User education
##### Get user education
*GET - **/user/:username/education***

**Sample request:**
```json
[
    {
        "id": 1,
        "from": "2015-09-01T00:00:00.000Z",
        "to": "2016-05-30T00:00:00.000Z",
        "qualification": "MS in Computer systems and networks",
        "course": "Computer systems and networks",
        "EducationalOrganization": {
            "id": 1,
            "name": "Ternopil Ivan Puluj National Technical University"
        }
    }
]
```

##### Add user education
*POST - **/user/:username/education***

**Sample request:**
```json
{
	"from": "2015-09-01",
	"to": "2016-05-30",
	"qualification": "MS in Computer systems and networks",
	"course": "Computer systems and networks",
	"organization": "Ternopil Ivan Puluj National Technical University"
}
```

**Sample response:**
```json
{
    "id": 1,
    "from": "2015-09-01T00:00:00.000Z",
    "to": "2016-05-30T00:00:00.000Z",
    "qualification": "MS in Computer systems and networks",
    "course": "Computer systems and networks",
    "EducationalOrganization": {
        "id": 1,
        "name": "Ternopil Ivan Puluj National Technical University"
    }
}
```

##### Update user education
*PUT - **/user/:username/education/:educationId***

**Sample request:**
```json
{
	"from": "2010-09-01",
	"to": "2012-05-30",
	"qualification": "MS in Computer systems and networks RENAMED",
	"course": "Computer systems and networks RENAMED",
	"organization": "Ternopil Ivan Puluj National Technical University RENAMED"
}
```

**Sample response:**
```json
{
    "id": 1,
    "from": "2010-09-01T00:00:00.000Z",
    "to": "2012-05-30T00:00:00.000Z",
    "qualification": "MS in Computer systems and networks RENAMED",
    "course": "Computer systems and networks RENAMED",
    "EducationalOrganization": {
        "id": 2,
        "name": "Ternopil Ivan Puluj National Technical University RENAMED"
    }
}
```

##### Delete user education
*DELETE - **/user/:username/education/:educationId***

No response for the route. Status 204 No Content on success.

#### User projects
##### Get user projects
*GET - **/user/:username/projects***

**Sample response:**
```json
[
    {
        "id": 1,
        "name": "Project-113",
        "Technologies": [
            {
                "id": 1,
                "name": "PHP5",
                "ProjectTechnology": {
                    "id": 1,
                    "createdAt": "2018-10-07T09:26:39.000Z",
                    "updatedAt": "2018-10-07T09:26:39.000Z",
                    "ProjectId": 1,
                    "TechnologyId": 1
                }
            },
            {
                "id": 2,
                "name": "JavaScript",
                "ProjectTechnology": {
                    "id": 2,
                    "createdAt": "2018-10-07T09:26:39.000Z",
                    "updatedAt": "2018-10-07T09:26:39.000Z",
                    "ProjectId": 1,
                    "TechnologyId": 2
                }
            },
            {
                "id": 3,
                "name": "Jquery",
                "ProjectTechnology": {
                    "id": 3,
                    "createdAt": "2018-10-07T09:26:39.000Z",
                    "updatedAt": "2018-10-07T09:26:39.000Z",
                    "ProjectId": 1,
                    "TechnologyId": 3
                }
            }
        ],
        "ProjectCharacteristics": [
            {
                "id": 1,
                "value": "The solution simplifies work and material resources planning;"
            },
            {
                "id": 2,
                "value": "Tasks scheduling as a reaction to an incident or as planned maintenance;"
            }
        ]
    }
]
```

##### Add user project
*POST - **/user/:username/projects***

**Sample request:**
```json
{
	"name": "Project-113",
	"technologies": [
		"PHP5",
		"JavaScript",
		"Jquery"
	],
	"characteristics": [
		"The solution simplifies work and material resources planning;",
		"Tasks scheduling as a reaction to an incident or as planned maintenance;"
	]
}
```

**Sample response:**
```json
{
    "id": 1,
    "name": "Project-113",
    "Technologies": [
        {
            "id": 1,
            "name": "PHP5",
            "ProjectTechnology": {
                "id": 1,
                "createdAt": "2018-10-07T09:26:39.000Z",
                "updatedAt": "2018-10-07T09:26:39.000Z",
                "ProjectId": 1,
                "TechnologyId": 1
            }
        },
        {
            "id": 2,
            "name": "JavaScript",
            "ProjectTechnology": {
                "id": 2,
                "createdAt": "2018-10-07T09:26:39.000Z",
                "updatedAt": "2018-10-07T09:26:39.000Z",
                "ProjectId": 1,
                "TechnologyId": 2
            }
        },
        {
            "id": 3,
            "name": "Jquery",
            "ProjectTechnology": {
                "id": 3,
                "createdAt": "2018-10-07T09:26:39.000Z",
                "updatedAt": "2018-10-07T09:26:39.000Z",
                "ProjectId": 1,
                "TechnologyId": 3
            }
        }
    ],
    "ProjectCharacteristics": [
        {
            "id": 1,
            "value": "The solution simplifies work and material resources planning;"
        },
        {
            "id": 2,
            "value": "Tasks scheduling as a reaction to an incident or as planned maintenance;"
        }
    ]
}
```

##### Update user project
*PUT - **/user/:username/projects/:projectId***

**Sample request:**
```json
{
	"name": "Project-updated",
	"technologies": [
		"PHP5",
		"update",
		"Jquery"
	],
	"characteristics": [
		"5",
		"5"
	]
}
```

**Sample response:**
```json
{
    "id": 1,
    "name": "Project-updated",
    "Technologies": [
        {
            "id": 1,
            "name": "PHP5",
            "ProjectTechnology": {
                "id": 1,
                "createdAt": "2018-10-07T09:26:39.000Z",
                "updatedAt": "2018-10-07T09:26:39.000Z",
                "ProjectId": 1,
                "TechnologyId": 1
            }
        },
        {
            "id": 3,
            "name": "Jquery",
            "ProjectTechnology": {
                "id": 3,
                "createdAt": "2018-10-07T09:26:39.000Z",
                "updatedAt": "2018-10-07T09:26:39.000Z",
                "ProjectId": 1,
                "TechnologyId": 3
            }
        },
        {
            "id": 4,
            "name": "update",
            "ProjectTechnology": {
                "id": 4,
                "createdAt": "2018-10-07T09:29:22.000Z",
                "updatedAt": "2018-10-07T09:29:22.000Z",
                "ProjectId": 1,
                "TechnologyId": 4
            }
        }
    ],
    "ProjectCharacteristics": [
        {
            "id": 3,
            "value": "5"
        },
        {
            "id": 4,
            "value": "5"
        }
    ]
}
```

##### Delete user project
*DELETE - **/user/:username/projects/:projectId***

No response for the route. Status 204 No Content on success.

#### User languages
##### Get user languages
*GET - **/user/:username/languages***

**Sample response:**
```json
[
    {
        "id": 1,
        "name": "English"
    }
]
```

##### Add user language
*POST - **/user/:username/languages***

**Sample request:**
```json
{
	"name": "English"
}
```

**Sample response:**
```json
{
    "id": 1,
    "name": "English"
}
```

##### Delete user language
*DELETE - **/user/:username/languages/:languageId***

No response for the route. Status 204 No Content on success.

#### User resumes
##### Get user resumes
*GET - **/user/:username/resumes***

**Sample response:**
```json
[
    {
        "id": 1,
        "name": "Resume-1",
        "summary": "Text summary",
        "Languages": [],
        "Education": [
            {
                "id": 1,
                "from": "2010-09-01T00:00:00.000Z",
                "to": "2012-05-30T00:00:00.000Z",
                "qualification": "MS in Computer systems and networks RENAMED",
                "course": "Computer systems and networks RENAMED",
                "ResumeEducation": {
                    "id": 1,
                    "createdAt": "2018-10-07T09:42:14.000Z",
                    "updatedAt": "2018-10-07T09:42:14.000Z",
                    "EducationId": 1,
                    "ResumeId": 1
                },
                "EducationalOrganization": {
                    "id": 2,
                    "name": "Ternopil Ivan Puluj National Technical University RENAMED"
                }
            }
        ],
        "Projects": [],
        "Experiences": [
            {
                "id": 1,
                "from": "2015-01-01T00:00:00.000Z",
                "to": "2018-12-01T00:00:00.000Z",
                "ResumeExperience": {
                    "id": 1,
                    "createdAt": "2018-10-07T09:42:14.000Z",
                    "updatedAt": "2018-10-07T09:42:14.000Z",
                    "ExperienceId": 1,
                    "ResumeId": 1
                },
                "Employer": {
                    "id": 2,
                    "name": "Vodokanal-renamed"
                },
                "JobPosition": {
                    "id": 2,
                    "name": "Web/UI developer-renamed"
                }
            }
        ],
        "Skills": [
            {
                "id": 1,
                "value": "PHP",
                "SkillType": {
                    "id": 1,
                    "type": "languages"
                },
                "ResumeSkill": {
                    "id": 1,
                    "createdAt": "2018-10-07T09:42:14.000Z",
                    "updatedAt": "2018-10-07T09:42:14.000Z",
                    "ResumeId": 1,
                    "SkillId": 1
                }
            },
            {
                "id": 2,
                "value": "JavaScript",
                "SkillType": {
                    "id": 1,
                    "type": "languages"
                },
                "ResumeSkill": {
                    "id": 2,
                    "createdAt": "2018-10-07T09:42:14.000Z",
                    "updatedAt": "2018-10-07T09:42:14.000Z",
                    "ResumeId": 1,
                    "SkillId": 2
                }
            }
        ],
        "Contacts": [
            {
                "id": 1,
                "value": "Lviv, Ukraine",
                "ResumeContact": {
                    "id": 1,
                    "createdAt": "2018-10-07T09:42:14.000Z",
                    "updatedAt": "2018-10-07T09:42:14.000Z",
                    "ContactId": 1,
                    "ResumeId": 1
                },
                "ContactType": {
                    "id": 1,
                    "type": "address"
                }
            },
            {
                "id": 2,
                "value": "demo-user@mail.com",
                "ResumeContact": {
                    "id": 2,
                    "createdAt": "2018-10-07T09:42:14.000Z",
                    "updatedAt": "2018-10-07T09:42:14.000Z",
                    "ContactId": 2,
                    "ResumeId": 1
                },
                "ContactType": {
                    "id": 3,
                    "type": "e-mail"
                }
            }
        ],
        "JobPosition": {
            "id": 3,
            "name": "Fullstack Web Developer"
        }
    }
]
```

##### Add user resume
*POST - **/user/:username/resumes***

**Sample request:**
```json
{
	"name": "Resume-1",
	"summary": "Text summary",
	"position": "Fullstack Web Developer",
	"contacts": [1, 2],
	"skills": [1, 2],
	"experience": [1, 2, 5],
	"projects": [2],
	"education": [1],
	"languages": [1]
}
```

**Sample response:**
```json
{
    "id": 1,
    "name": "Resume-1",
    "summary": "Text summary",
    "Languages": [],
    "Education": [
        {
            "id": 1,
            "from": "2010-09-01T00:00:00.000Z",
            "to": "2012-05-30T00:00:00.000Z",
            "qualification": "MS in Computer systems and networks RENAMED",
            "course": "Computer systems and networks RENAMED",
            "ResumeEducation": {
                "id": 1,
                "createdAt": "2018-10-07T09:42:14.000Z",
                "updatedAt": "2018-10-07T09:42:14.000Z",
                "EducationId": 1,
                "ResumeId": 1
            },
            "EducationalOrganization": {
                "id": 2,
                "name": "Ternopil Ivan Puluj National Technical University RENAMED"
            }
        }
    ],
    "Projects": [],
    "Experiences": [
        {
            "id": 1,
            "from": "2015-01-01T00:00:00.000Z",
            "to": "2018-12-01T00:00:00.000Z",
            "ResumeExperience": {
                "id": 1,
                "createdAt": "2018-10-07T09:42:14.000Z",
                "updatedAt": "2018-10-07T09:42:14.000Z",
                "ExperienceId": 1,
                "ResumeId": 1
            },
            "Employer": {
                "id": 2,
                "name": "Vodokanal-renamed"
            },
            "JobPosition": {
                "id": 2,
                "name": "Web/UI developer-renamed"
            }
        }
    ],
    "Skills": [
        {
            "id": 1,
            "value": "PHP",
            "SkillType": {
                "id": 1,
                "type": "languages"
            },
            "ResumeSkill": {
                "id": 1,
                "createdAt": "2018-10-07T09:42:14.000Z",
                "updatedAt": "2018-10-07T09:42:14.000Z",
                "ResumeId": 1,
                "SkillId": 1
            }
        },
        {
            "id": 2,
            "value": "JavaScript",
            "SkillType": {
                "id": 1,
                "type": "languages"
            },
            "ResumeSkill": {
                "id": 2,
                "createdAt": "2018-10-07T09:42:14.000Z",
                "updatedAt": "2018-10-07T09:42:14.000Z",
                "ResumeId": 1,
                "SkillId": 2
            }
        }
    ],
    "Contacts": [
        {
            "id": 1,
            "value": "Lviv, Ukraine",
            "ResumeContact": {
                "id": 1,
                "createdAt": "2018-10-07T09:42:14.000Z",
                "updatedAt": "2018-10-07T09:42:14.000Z",
                "ContactId": 1,
                "ResumeId": 1
            },
            "ContactType": {
                "id": 1,
                "type": "address"
            }
        },
        {
            "id": 2,
            "value": "demo-user@mail.com",
            "ResumeContact": {
                "id": 2,
                "createdAt": "2018-10-07T09:42:14.000Z",
                "updatedAt": "2018-10-07T09:42:14.000Z",
                "ContactId": 2,
                "ResumeId": 1
            },
            "ContactType": {
                "id": 3,
                "type": "e-mail"
            }
        }
    ],
    "JobPosition": {
        "id": 3,
        "name": "Fullstack Web Developer"
    }
}
```

##### Update user resume
*PUT - **/user/:username/resumes/:resumeId***

**Sample request:**
```json
{
	"name": "Resume-1-updated",
	"summary": "Text summary-updated",
	"position": "Fullstack Web Developer",
	"contacts": [1, 2],
	"skills": [1, 2],
	"experience": [],
	"projects": [],
	"education": [],
	"languages": []
}
```

**Sample response:**
```json
{
    "id": 1,
    "name": "Resume-1-updated",
    "summary": "Text summary-updated",
    "Languages": [],
    "Education": [],
    "Projects": [],
    "Experiences": [],
    "Skills": [
        {
            "id": 1,
            "value": "PHP",
            "SkillType": {
                "id": 1,
                "type": "languages"
            },
            "ResumeSkill": {
                "id": 1,
                "createdAt": "2018-10-07T09:42:14.000Z",
                "updatedAt": "2018-10-07T09:42:14.000Z",
                "ResumeId": 1,
                "SkillId": 1
            }
        },
        {
            "id": 2,
            "value": "JavaScript",
            "SkillType": {
                "id": 1,
                "type": "languages"
            },
            "ResumeSkill": {
                "id": 2,
                "createdAt": "2018-10-07T09:42:14.000Z",
                "updatedAt": "2018-10-07T09:42:14.000Z",
                "ResumeId": 1,
                "SkillId": 2
            }
        }
    ],
    "Contacts": [
        {
            "id": 1,
            "value": "Lviv, Ukraine",
            "ResumeContact": {
                "id": 1,
                "createdAt": "2018-10-07T09:42:14.000Z",
                "updatedAt": "2018-10-07T09:42:14.000Z",
                "ContactId": 1,
                "ResumeId": 1
            },
            "ContactType": {
                "id": 1,
                "type": "address"
            }
        },
        {
            "id": 2,
            "value": "demo-user@mail.com",
            "ResumeContact": {
                "id": 2,
                "createdAt": "2018-10-07T09:42:14.000Z",
                "updatedAt": "2018-10-07T09:42:14.000Z",
                "ContactId": 2,
                "ResumeId": 1
            },
            "ContactType": {
                "id": 3,
                "type": "e-mail"
            }
        }
    ],
    "JobPosition": {
        "id": 3,
        "name": "Fullstack Web Developer"
    }
}
```

##### Delete user resume
*DELETE - **/user/:username/resumes/:resumeId***

No response for the route. Status 204 No Content on success.