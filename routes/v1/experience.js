const express = require('express');
const router = express.Router();
const experienceController = require('../../controllers/experience');

router.route('/user/:username/experience')
  .get(experienceController.get)
  .post(experienceController.create);

router.route('/user/:username/experience/:experienceId')
  .put(experienceController.update)
  .delete(experienceController.remove);

module.exports = router;