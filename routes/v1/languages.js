const express = require('express');
const router = express.Router();
const controller = require('../../controllers/languages');

router.route('/user/:username/languages')
  .get(controller.get)
  .post(controller.create);

router.route('/user/:username/languages/:languageId')
  .delete(controller.remove);

module.exports = router;