const express = require('express');
const router = express.Router();
const userController = require('../../controllers/user');

router.route('/user/:username')
  .get(userController.get)
  .put(userController.update)
  .delete(userController.remove);

router.post('/user', userController.create);

module.exports = router;