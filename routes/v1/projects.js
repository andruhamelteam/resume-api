const express = require('express');
const router = express.Router();
const controller = require('../../controllers/projects');

router.route('/user/:username/projects')
  .get(controller.get)
  .post(controller.create);

router.route('/user/:username/projects/:projectId')
  .put(controller.update)
  .delete(controller.remove);

module.exports = router;