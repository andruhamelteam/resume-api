const express = require('express');
const router = express.Router();
const controller = require('../../controllers/resume');

router.route('/user/:username/resumes')
  .get(controller.get)
  .post(controller.create);

router.route('/user/:username/resumes/:resumeId')
  .get(controller.getOne)
  .put(controller.update)
  .delete(controller.remove);

module.exports = router;