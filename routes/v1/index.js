const express = require('express');
const router = express.Router();
const userMiddleware = require('../../middleware/user');
const cors = require('cors');

router.use('/v1/user/:username', cors(), userMiddleware);

router.use('/v1',
  require('./user'),
  require('./contacts'),
  require('./skills'),
  require('./experience'),
  require('./education'),
  require('./projects'),
  require('./languages'),
  require('./resumes')
);

module.exports = router;