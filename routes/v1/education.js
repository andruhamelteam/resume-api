const express = require('express');
const router = express.Router();
const controller = require('../../controllers/education');

router.route('/user/:username/education')
  .get(controller.get)
  .post(controller.create);

router.route('/user/:username/education/:educationId')
  .put(controller.update)
  .delete(controller.remove);

module.exports = router;