const express = require('express');
const router = express.Router();
const skillsController = require('../../controllers/skills');

router.route('/user/:username/skills')
  .get(skillsController.get)
  .post(skillsController.create);

router.route('/user/:username/skills/:skillId')
  .put(skillsController.update)
  .delete(skillsController.remove);

module.exports = router;