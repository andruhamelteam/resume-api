const express = require('express');
const router = express.Router();
const contactsController = require('../../controllers/contacts');

router.route('/user/:username/contacts')
  .get(contactsController.get)
  .post(contactsController.create);

router.route('/user/:username/contacts/:contactId')
  .put(contactsController.update)
  .delete(contactsController.remove);

module.exports = router;